﻿using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data.Mapping
{
    public class PersonMap : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.HasKey(s => s.Id);
            builder.Property(s => s.LastName)
                .HasMaxLength(50)
                .IsRequired();
            builder.Property(s => s.FirstMidName)
                .HasMaxLength(50)
                .HasColumnName("FirstName")
                .IsRequired();
        }
    }
}