﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;

namespace Infrastructure.Data
{
    public class ContextSeed
    {
        private static Student[] _students;
        private static Instructor[] _instructors;
        private static Department[] _departments;
        private static Course[] _courses;

        public static async Task SeedAsync(SchoolContext context)
        {
            if (!context.Students.Any())
            {
                context.Students.AddRange(SeedStudents());
                await context.SaveChangesAsync();
            }

            if (!context.Instructors.Any())
            {
                context.Instructors.AddRange(SeedInstructors());
                await context.SaveChangesAsync();
            }

            if (!context.Departments.Any())
            {
                context.Departments.AddRange(SeedDepartments());
                await context.SaveChangesAsync();
            }

            if (!context.Courses.Any())
            {
                context.Courses.AddRange(SeedCourses());
                await context.SaveChangesAsync();
            }

            if (!context.OfficeAssignments.Any())
            {
                context.OfficeAssignments.AddRange(SeedOfficeAssignments());
                await context.SaveChangesAsync();
            }

            if (!context.CourseAssignments.Any())
            {
                context.CourseAssignments.AddRange(SeedCourseAssignments());
                await context.SaveChangesAsync();
            }

            if (!context.Enrollments.Any())
            {
                context.Enrollments.AddRange(SeedEnrollments());
                await context.SaveChangesAsync();
            }
        }

        private static IEnumerable<Student> SeedStudents()
        {
            _students = new[]
            {
                new Student
                {
                    FirstMidName = "Carson",
                    LastName = "Alexander",
                    EnrollmentDate = DateTime.Parse("2010-09-01")
                },
                new Student
                {
                    FirstMidName = "Meredith",
                    LastName = "Alonso",
                    EnrollmentDate = DateTime.Parse("2012-09-01")
                },
                new Student
                {
                    FirstMidName = "Arturo",
                    LastName = "Anand",
                    EnrollmentDate = DateTime.Parse("2013-09-01")
                },
                new Student
                {
                    FirstMidName = "Gytis",
                    LastName = "Barzdukas",
                    EnrollmentDate = DateTime.Parse("2012-09-01")
                },
                new Student
                {
                    FirstMidName = "Yan",
                    LastName = "Li",
                    EnrollmentDate = DateTime.Parse("2012-09-01")
                },
                new Student
                {
                    FirstMidName = "Peggy",
                    LastName = "Justice",
                    EnrollmentDate = DateTime.Parse("2011-09-01")
                },
                new Student
                {
                    FirstMidName = "Laura",
                    LastName = "Norman",
                    EnrollmentDate = DateTime.Parse("2013-09-01")
                },
                new Student
                {
                    FirstMidName = "Nino",
                    LastName = "Olivetto",
                    EnrollmentDate = DateTime.Parse("2005-09-01")
                }
            };
            return _students;
        }

        private static IEnumerable<Instructor> SeedInstructors()
        {
            _instructors = new[]
            {
                new Instructor
                {
                    FirstMidName = "Kim",
                    LastName = "Abercrombie",
                    HireDate = DateTime.Parse("1995-03-11")
                },
                new Instructor
                {
                    FirstMidName = "Fadi",
                    LastName = "Fakhouri",
                    HireDate = DateTime.Parse("2002-07-06")
                },
                new Instructor
                {
                    FirstMidName = "Roger",
                    LastName = "Harui",
                    HireDate = DateTime.Parse("1998-07-01")
                },
                new Instructor
                {
                    FirstMidName = "Candace",
                    LastName = "Kapoor",
                    HireDate = DateTime.Parse("2001-01-15")
                },
                new Instructor
                {
                    FirstMidName = "Roger",
                    LastName = "Zheng",
                    HireDate = DateTime.Parse("2004-02-12")
                }
            };
            return _instructors;
        }

        private static IEnumerable<Department> SeedDepartments()
        {
            _departments = new[]
            {
                new Department
                {
                    Name = "English",
                    Budget = 350000,
                    StartDate = DateTime.Parse("2007-09-01"),
                    InstructorID = _instructors.Single(i => i.LastName == "Abercrombie").Id
                },
                new Department
                {
                    Name = "Mathematics",
                    Budget = 100000,
                    StartDate = DateTime.Parse("2007-09-01"),
                    InstructorID = _instructors.Single(i => i.LastName == "Fakhouri").Id
                },
                new Department
                {
                    Name = "Engineering",
                    Budget = 350000,
                    StartDate = DateTime.Parse("2007-09-01"),
                    InstructorID = _instructors.Single(i => i.LastName == "Harui").Id
                },
                new Department
                {
                    Name = "Economics",
                    Budget = 100000,
                    StartDate = DateTime.Parse("2007-09-01"),
                    InstructorID = _instructors.Single(i => i.LastName == "Kapoor").Id
                }
            };
            return _departments;
        }

        private static IEnumerable<Course> SeedCourses()
        {
            _courses = new[]
            {
                new Course
                {
                    Id = 1050,
                    Title = "Chemistry",
                    Credits = 3,
                    DepartmentID = _departments.Single(s => s.Name == "Engineering").Id
                },
                new Course
                {
                    Id = 4022,
                    Title = "Microeconomics",
                    Credits = 3,
                    DepartmentID = _departments.Single(s => s.Name == "Economics").Id
                },
                new Course
                {
                    Id = 4041,
                    Title = "Macroeconomics",
                    Credits = 3,
                    DepartmentID = _departments.Single(s => s.Name == "Economics").Id
                },
                new Course
                {
                    Id = 1045,
                    Title = "Calculus",
                    Credits = 4,
                    DepartmentID = _departments.Single(s => s.Name == "Mathematics").Id
                },
                new Course
                {
                    Id = 3141,
                    Title = "Trigonometry",
                    Credits = 4,
                    DepartmentID = _departments.Single(s => s.Name == "Mathematics").Id
                },
                new Course
                {
                    Id = 2021,
                    Title = "Composition",
                    Credits = 3,
                    DepartmentID = _departments.Single(s => s.Name == "English").Id
                },
                new Course
                {
                    Id = 2042,
                    Title = "Literature",
                    Credits = 4,
                    DepartmentID = _departments.Single(s => s.Name == "English").Id
                }
            };
            return _courses;
        }

        private static IEnumerable<OfficeAssignment> SeedOfficeAssignments()
        {
            return new[]
            {
                new OfficeAssignment
                {
                    InstructorID = _instructors.Single(i => i.LastName == "Fakhouri").Id,
                    Location = "Smith 17"
                },
                new OfficeAssignment
                {
                    InstructorID = _instructors.Single(i => i.LastName == "Harui").Id,
                    Location = "Gowan 27"
                },
                new OfficeAssignment
                {
                    InstructorID = _instructors.Single(i => i.LastName == "Kapoor").Id,
                    Location = "Thompson 304"
                },
            };
        }

        private static IEnumerable<CourseAssignment> SeedCourseAssignments()
        {
            return new[]
            {
                new CourseAssignment
                {
                    CourseId = _courses.Single(c => c.Title == "Chemistry").Id,
                    InstructorId = _instructors.Single(i => i.LastName == "Kapoor").Id
                },
                new CourseAssignment
                {
                    CourseId = _courses.Single(c => c.Title == "Chemistry").Id,
                    InstructorId = _instructors.Single(i => i.LastName == "Harui").Id
                },
                new CourseAssignment
                {
                    CourseId = _courses.Single(c => c.Title == "Microeconomics").Id,
                    InstructorId = _instructors.Single(i => i.LastName == "Zheng").Id
                },
                new CourseAssignment
                {
                    CourseId = _courses.Single(c => c.Title == "Macroeconomics").Id,
                    InstructorId = _instructors.Single(i => i.LastName == "Zheng").Id
                },
                new CourseAssignment
                {
                    CourseId = _courses.Single(c => c.Title == "Calculus").Id,
                    InstructorId = _instructors.Single(i => i.LastName == "Fakhouri").Id
                },
                new CourseAssignment
                {
                    CourseId = _courses.Single(c => c.Title == "Trigonometry").Id,
                    InstructorId = _instructors.Single(i => i.LastName == "Harui").Id
                },
                new CourseAssignment
                {
                    CourseId = _courses.Single(c => c.Title == "Composition").Id,
                    InstructorId = _instructors.Single(i => i.LastName == "Abercrombie").Id
                },
                new CourseAssignment
                {
                    CourseId = _courses.Single(c => c.Title == "Literature").Id,
                    InstructorId = _instructors.Single(i => i.LastName == "Abercrombie").Id
                },
            };
        }

        private static IEnumerable<Enrollment> SeedEnrollments()
        {
            return new[]
            {
                new Enrollment
                {
                    StudentId = _students.Single(s => s.LastName == "Alexander").Id,
                    CourseId = _courses.Single(c => c.Title == "Chemistry").Id,
                    Grade = Grade.A
                },
                new Enrollment
                {
                    StudentId = _students.Single(s => s.LastName == "Alexander").Id,
                    CourseId = _courses.Single(c => c.Title == "Microeconomics").Id,
                    Grade = Grade.C
                },
                new Enrollment
                {
                    StudentId = _students.Single(s => s.LastName == "Alexander").Id,
                    CourseId = _courses.Single(c => c.Title == "Macroeconomics").Id,
                    Grade = Grade.B
                },
                new Enrollment
                {
                    StudentId = _students.Single(s => s.LastName == "Alonso").Id,
                    CourseId = _courses.Single(c => c.Title == "Calculus").Id,
                    Grade = Grade.B
                },
                new Enrollment
                {
                    StudentId = _students.Single(s => s.LastName == "Alonso").Id,
                    CourseId = _courses.Single(c => c.Title == "Trigonometry").Id,
                    Grade = Grade.B
                },
                new Enrollment
                {
                    StudentId = _students.Single(s => s.LastName == "Alonso").Id,
                    CourseId = _courses.Single(c => c.Title == "Composition").Id,
                    Grade = Grade.B
                },
                new Enrollment
                {
                    StudentId = _students.Single(s => s.LastName == "Anand").Id,
                    CourseId = _courses.Single(c => c.Title == "Chemistry").Id
                },
                new Enrollment
                {
                    StudentId = _students.Single(s => s.LastName == "Anand").Id,
                    CourseId = _courses.Single(c => c.Title == "Microeconomics").Id,
                    Grade = Grade.B
                },
                new Enrollment
                {
                    StudentId = _students.Single(s => s.LastName == "Barzdukas").Id,
                    CourseId = _courses.Single(c => c.Title == "Chemistry").Id,
                    Grade = Grade.B
                },
                new Enrollment
                {
                    StudentId = _students.Single(s => s.LastName == "Li").Id,
                    CourseId = _courses.Single(c => c.Title == "Composition").Id,
                    Grade = Grade.B
                },
                new Enrollment
                {
                    StudentId = _students.Single(s => s.LastName == "Justice").Id,
                    CourseId = _courses.Single(c => c.Title == "Literature").Id,
                    Grade = Grade.B
                }
            };
        }
    }
}