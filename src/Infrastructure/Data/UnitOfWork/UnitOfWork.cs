﻿using System;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repositories.Base;
using ApplicationCore.Interfaces.UnitOfWork;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SchoolContext _context;
        private readonly IServiceProvider _serviceProvider;

        public UnitOfWork(SchoolContext context, IServiceProvider serviceProvider)
        {
            _context = context;
            _serviceProvider = serviceProvider;
        }

        public IGenericRepository<TEntity> GetRepository<TEntity>()
            where TEntity : BaseEntity
        {
            return _serviceProvider.GetRequiredService<IGenericRepository<TEntity>>();
        }

        public TRepository GetRepositoryWithTypeRepository<TEntity, TRepository>()
            where TEntity : BaseEntity
            where TRepository : IGenericRepository<TEntity>
        {
            return _serviceProvider.GetRequiredService<TRepository>();
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}