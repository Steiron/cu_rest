﻿using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces.PagedList;
using ApplicationCore.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data.Repositories
{
    public class StudentRepository : GenericRepository<Student>, IStudentRepository
    {
        public StudentRepository(SchoolContext dbContext) : base(dbContext)
        {
        }

        /// <inheritdoc />
        public async Task<Student> GetStudentByIdWithEnrollmentsAndCourseAsync(int id)
        {
            return await GetFirstOrDefaultAsync(s => s.Id == id,
                include: source => source
                    .Include(s => s.Enrollments)
                    .ThenInclude(e => e.Course));
        }

        public async Task<IPagedList<Student>> GetAllWithFilter(string sortOrder, string searchString, int page,
            int pageSize)
        {
            var students = _dbContext.Students.AsQueryable();

            if (!string.IsNullOrEmpty(searchString))
            {
                students = students.Where(s => s.LastName.Contains(searchString)
                                               || s.FirstMidName.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    students = students.OrderByDescending(s => s.LastName);
                    break;
                case "date":
                    students = students.OrderBy(s => s.EnrollmentDate);
                    break;
                case "date_desc":
                    students = students.OrderByDescending(s => s.EnrollmentDate);
                    break;
                default:
                    students = students.OrderBy(s => s.LastName);
                    break;
            }

            return await students.AsNoTracking().ToPagedListAsync(page, pageSize);
        }
    }
}