﻿namespace ApplicationCore.DTO
{
    public class CourseAssignmentDto
    {
        public CourseDto Course { get; set; }
    }
}