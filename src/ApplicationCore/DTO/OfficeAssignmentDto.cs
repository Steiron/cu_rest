﻿namespace ApplicationCore.DTO
{
    public class OfficeAssignmentDto
    {
        public string Location { get; set; }
    }
}