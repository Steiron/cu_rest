﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ApplicationCore.DTO
{
    public class CourseDto
    {
        [Required] public int Id { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Title { get; set; }

        [Required] [Range(0, 5)] public int Credits { get; set; }

        [Required] public int DepartmentID { get; set; }

        public DepartmentDto Department { get; set; }
        public ICollection<EnrollmentDto> Enrollments { get; set; }
        public ICollection<CourseAssignmentDto> CourseAssignments { get; set; }
    }
}