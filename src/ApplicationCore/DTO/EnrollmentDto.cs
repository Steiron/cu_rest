﻿using ApplicationCore.DTO.Student;
using ApplicationCore.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ApplicationCore.DTO
{
    public class EnrollmentDto : BaseDto
    {
        public int CourseId { get; set; }
        public int StudentId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Grade? Grade { get; set; }

        public CourseDto Course { get; set; }
        public StudentDto Student { get; set; }
    }
}