﻿using System;
using System.Collections.Generic;
using ApplicationCore.DTO.Student;
using Newtonsoft.Json;

namespace ApplicationCore.DTO
{
    public class InstructorDto : PersonDto
    {
        [JsonConverter(typeof(DateTimeConverter), "yyyy-MM-dd")]
        public DateTime HireDate { get; set; }

        public ICollection<CourseAssignmentDto> CourseAssignments { get; set; }
        public OfficeAssignmentDto OfficeAssignment { get; set; }
    }
}