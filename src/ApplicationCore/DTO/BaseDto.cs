﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace ApplicationCore.DTO
{
    public class BaseDto
    {
        public int Id { get; set; }
    }
}