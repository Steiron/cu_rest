﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ApplicationCore.DTO.Student
{
    public class StudentDto : PersonDto
    {
        [JsonConverter(typeof(DateTimeConverter), "yyyy-MM-dd")]
        public DateTime EnrollmentDate { get; set; }
    }

    class DateTimeConverter : IsoDateTimeConverter
    {
        public DateTimeConverter(string format)
        {
            DateTimeFormat = format;
        }
    }
}