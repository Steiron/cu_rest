﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ApplicationCore.Entities;

namespace ApplicationCore.DTO.Student
{
    public class StudentEnrollmentsDto : StudentDto
    {
        public ICollection<EnrollmentDto> Enrollments { get; set; }
    }
}