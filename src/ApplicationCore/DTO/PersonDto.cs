﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ApplicationCore.DTO
{
    public class PersonDto : BaseDto
    {
        [Required]
        [MaxLength(50, ErrorMessage = "Фамилия не должно привышать 50 символов")]
        [JsonProperty("last")]
        public string LastName { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Значение не должно привышать 50 символов")]
        [JsonProperty("first")]
        public string FirstMidName { get; set; }

    }
}