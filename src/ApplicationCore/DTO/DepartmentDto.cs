﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ApplicationCore.DTO.Student;
using Newtonsoft.Json;

namespace ApplicationCore.DTO
{
    public class DepartmentDto : BaseDto
    {
        [StringLength(50, MinimumLength = 3)] public string Name { get; set; }
        public decimal Budget { get; set; }

        [JsonConverter(typeof(DateTimeConverter), "yyyy-MM-dd")]
        public DateTime StartDate { get; set; }

        public int? InstructorID { get; set; }


        public InstructorDto Administrator { get; set; }
        public ICollection<CourseDto> Courses { get; set; }
    }
}