﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repositories.Base;
using ApplicationCore.Interfaces.Services;
using ApplicationCore.Interfaces.UnitOfWork;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace ApplicationCore.Services
{
    public class InstructorService : IInstructorService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IGenericRepository<Instructor> _repository;

        public InstructorService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _repository = unitOfWork.GetRepository<Instructor>();
        }

        public async Task<IEnumerable<InstructorDto>> GetAllWithCoursesAsync()
        {
            return _mapper.Map<IEnumerable<InstructorDto>>(await _repository.GetAllAsync(
                include: i => i.Include(c => c.OfficeAssignment)
                    .Include(c => c.CourseAssignments)
                    .ThenInclude(c => c.Course),
                orderBy: o => o.OrderBy(i => i.LastName)));
        }

        public async Task<InstructorDto> GetById(int id)
        {
            return _mapper.Map<InstructorDto>(await _repository.GetFirstOrDefaultAsync(i => i.Id == id));
        }
    }
}