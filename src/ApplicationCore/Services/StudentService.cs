﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.DTO.Student;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repositories;
using ApplicationCore.Interfaces.Services;
using ApplicationCore.Interfaces.UnitOfWork;
using AutoMapper;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces.PagedList;

namespace ApplicationCore.Services
{
    public class StudentService : ISudentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IStudentRepository _repository;
        private readonly IMapper _mapper;

        public StudentService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.GetRepositoryWithTypeRepository<Student, IStudentRepository>();
            _mapper = mapper;
        }

        public async Task<StudentDto> GetByIdAsync(int id)
        {
            return _mapper.Map<StudentDto>(await _repository.GetByIdAsync(id));
        }

        /// <inheritdoc />
        public async Task<StudentEnrollmentsDto> GetStudentByIdWithEnrollmentsAsync(int id)
        {
            return _mapper.Map<StudentEnrollmentsDto>(
                await _repository.GetStudentByIdWithEnrollmentsAndCourseAsync(id));
        }

        /// <inheritdoc />
        public async Task<IPagedList<StudentDto>> GetPagedListAsync(string sortOrder, string searchString,
            int? page, int pageSize)
        {
            return _mapper.Map<PagedList<StudentDto>>(
                await _repository.GetAllWithFilter(sortOrder, searchString, page ?? 1, pageSize));
        }

        public async Task<StudentDto> CreateStudentAsync(StudentDto studentDto)
        {
            Student student = studentDto.ToStudent(new Student());

            await _repository.AddAsync(student);
            await _unitOfWork.SaveAsync();

            return _mapper.Map<StudentDto>(student);
        }

        public async Task<StudentDto> UpdateStudentAsync(int id, StudentDto studentDto)
        {
            Student student = await _repository.GetByIdAsync(id);
            if (student == null) return null;

            student = studentDto.ToStudent(student);

            _repository.Update(student);
            await _unitOfWork.SaveAsync();

            return _mapper.Map<StudentDto>(student);
        }

        public async Task<StudentDto> DeleteStudentAsync(int id)
        {
            Student student = await _repository.GetByIdAsync(id);
            if (student == null) return null;

            _repository.Delete(student);
            await _unitOfWork.SaveAsync();

            return _mapper.Map<StudentDto>(student);
        }
    }
}