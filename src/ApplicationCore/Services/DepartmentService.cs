﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces.Repositories.Base;
using ApplicationCore.Interfaces.Services;
using ApplicationCore.Interfaces.UnitOfWork;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace ApplicationCore.Services
{
    public class DepartmentService : IDepartmentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IGenericRepository<Department> _repository;

        public DepartmentService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _repository = unitOfWork.GetRepository<Department>();
        }

        public async Task<IList<DepartmentDto>> GetAllWithAdministratorAsync()
        {
            return _mapper.Map<IList<DepartmentDto>>(
                await _repository.GetAllAsync(include: i => i.Include(d => d.Administrator)));
        }

        public async Task<DepartmentDto> GetById(int id)
        {
            return _mapper.Map<DepartmentDto>(await _repository.GetFirstOrDefaultAsync(d => d.Id == id));
        }

        public async Task<DepartmentDto> GetDetailsById(int id)
        {
            return _mapper.Map<DepartmentDto>(await _repository.GetFirstOrDefaultAsync(
                d => d.Id == id,
                include: i => i.Include(d => d.Administrator)));
        }

        public async Task<DepartmentDto> CreateDepartmentAsync(DepartmentDto departmentDto)
        {
            Department department = departmentDto.ToDepartment(new Department());
            await _repository.AddAsync(department);
            await _unitOfWork.SaveAsync();
            return _mapper.Map<DepartmentDto>(department);
        }

        public async Task<DepartmentDto> UpdateDepartmentAsync(int id, DepartmentDto departmentDto)
        {
            Department department = await _repository.GetFirstOrDefaultAsync(d => d.Id == id,
                include: i => i.Include(d => d.Administrator), disableTracking: false);

            if (department == null) return null;
            department = departmentDto.ToDepartment(department);
            _repository.Update(department);
            await _unitOfWork.SaveAsync();
            return _mapper.Map<DepartmentDto>(department);
        }

        public async Task<DepartmentDto> DeleteDepartmentAsync(int id)
        {
            Department department = await _repository.GetFirstOrDefaultAsync(d => d.Id == id);
            if (department == null) return null;
            _repository.Delete(department);
            await _unitOfWork.SaveAsync();
            return _mapper.Map<DepartmentDto>(department);
        }
    }
}