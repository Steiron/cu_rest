﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Entities;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces.Repositories;
using ApplicationCore.Interfaces.Repositories.Base;
using ApplicationCore.Interfaces.Services;
using ApplicationCore.Interfaces.UnitOfWork;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace ApplicationCore.Services
{
    public class CourseService : ICourseService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IGenericRepository<Course> _repository;

        public CourseService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _repository = unitOfWork.GetRepository<Course>();
        }

        public async Task<IList<CourseDto>> GetAllWithDepartmentAsync()
        {
            return _mapper.Map<IList<CourseDto>>(await _repository.GetAllAsync(
                include: i => i.Include(c => c.Department),
                orderBy: o => o.OrderBy(c => c.Id)));
        }

        public async Task<CourseDto> GetByIdWithDepartmentAsync(int id)
        {
            return _mapper.Map<CourseDto>(await _repository.GetFirstOrDefaultAsync(c => c.Id == id,
                include: i => i.Include(c => c.Department)));
        }

        public async Task<CourseDto> GetById(int id)
        {
            return _mapper.Map<CourseDto>(await _repository.GetFirstOrDefaultAsync(c => c.Id == id));
        }

        public async Task<CourseDto> CreateCourseAsync(CourseDto courseDto)
        {
            Course course = courseDto.ToCourse(new Course());
            await _repository.AddAsync(course);
            await _unitOfWork.SaveAsync();
            return _mapper.Map<CourseDto>(course);
        }

        public async Task<CourseDto> UpdateCourseAsync(int id, CourseDto courseDto)
        {
            Course course = await _repository.GetFirstOrDefaultAsync(c => c.Id == id, disableTracking: false);
            if (course == null) return null;
            course = courseDto.ToCourseUpdate(course);
            _repository.Update(course);
            await _unitOfWork.SaveAsync();
            return _mapper.Map<CourseDto>(course);
        }

        public async Task<CourseDto> DeleteCourseAsync(int id)
        {
            Course course = await _repository.GetFirstOrDefaultAsync(c => c.Id == id);
            if (course == null) return null;
            _repository.Delete(course);
            await _unitOfWork.SaveAsync();
            return _mapper.Map<CourseDto>(course);
        }
    }
}