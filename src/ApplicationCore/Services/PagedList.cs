﻿using System;
using System.Collections.Generic;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.PagedList;

namespace ApplicationCore.Services
{
    public class PagedList<TEntity> : IPagedList<TEntity>
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public int TotalPages { get; set; }
        public IList<TEntity> Items { get; set; }

        public PagedList(IList<TEntity> items, int pageIndex, int pageSize, int totalCount)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
            TotalCount = totalCount;
            TotalPages = (int) Math.Ceiling(totalCount / (double) pageSize);
            Items = items;
        }
    }
}