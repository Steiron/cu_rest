﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.PagedList;
using ApplicationCore.Services;
using Microsoft.EntityFrameworkCore;

namespace ApplicationCore.Extensions
{
    public static class QueryablePageListExtensions
    {
        /// <summary>
        /// Преобразовывает IQueryable в <see cref="IPagedList{TEntity}"/> с заданными количеством <paramref name="pageSize"/> на странице <paramref name="pageIndex"/>
        /// и возвращает выбранные данные в виде списка
        /// </summary>
        /// <param name="source"></param>
        /// <param name="pageIndex">Номер страницы</param>
        /// <param name="pageSize">Количество элементов на странице</param>
        /// <param name="cancellationToken"><see cref="CancellationToken" /> </param>
        /// <returns></returns>
        public static async Task<IPagedList<TEntity>> ToPagedListAsync<TEntity>(
            this IQueryable<TEntity> source,
            int pageIndex,
            int pageSize,
            CancellationToken cancellationToken = default(CancellationToken)) where TEntity : BaseEntity
        {
            var count = await source.CountAsync(cancellationToken).ConfigureAwait(false);
            var items = await source.Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync(cancellationToken)
                .ConfigureAwait(false);

            return new PagedList<TEntity>(items, pageIndex, pageSize, count);
        }
    }
}