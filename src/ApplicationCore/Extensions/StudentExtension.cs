﻿using ApplicationCore.DTO;
using ApplicationCore.DTO.Student;
using ApplicationCore.Entities;

namespace ApplicationCore.Extensions
{
    public static class StudentExtension
    {
        public static Student ToStudent(this StudentDto studentDto, Student student)
        {
            student.LastName = studentDto.LastName;
            student.FirstMidName = studentDto.FirstMidName;
            student.EnrollmentDate = studentDto.EnrollmentDate;
            return student;
        }
    }
}