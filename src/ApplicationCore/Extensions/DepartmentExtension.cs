﻿using ApplicationCore.DTO;
using ApplicationCore.Entities;

namespace ApplicationCore.Extensions
{
    public static class DepartmentExtension
    {
        public static Department ToDepartment(this DepartmentDto departmentDto, Department department)
        {
            department.Budget = departmentDto.Budget;
            department.InstructorID = departmentDto.InstructorID;
            department.Name = departmentDto.Name;
            department.StartDate = departmentDto.StartDate;
            return department;
        }
    }
}