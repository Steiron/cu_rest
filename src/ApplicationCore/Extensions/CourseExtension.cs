﻿using ApplicationCore.DTO;
using ApplicationCore.Entities;

namespace ApplicationCore.Extensions
{
    public static class CourseExtension
    {
        public static Course ToCourse(this CourseDto courseDto, Course course)
        {
            course.Id = courseDto.Id;
            course.Credits = courseDto.Credits;
            course.DepartmentID = courseDto.DepartmentID;
            course.Title = courseDto.Title;
            return course;
        }

        public static Course ToCourseUpdate(this CourseDto courseDto, Course course)
        {
            course.Credits = courseDto.Credits;
            course.DepartmentID = courseDto.DepartmentID;
            course.Title = courseDto.Title;
            return course;
        }
    }
}