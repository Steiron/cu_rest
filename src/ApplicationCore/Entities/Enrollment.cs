﻿namespace ApplicationCore.Entities
{
    public enum Grade
    {
        A,
        B,
        C,
        D,
        F
    }

    public class Enrollment : BaseEntity
    {
        public int CourseId { get; set; }
        public int StudentId { get; set; }
        public Grade? Grade { get; set; }

        public Course Course { get; set; }
        public Student Student { get; set; }
    }
}