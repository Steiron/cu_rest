﻿using System.ComponentModel.DataAnnotations;

namespace ApplicationCore.Entities
{
    public class Person : BaseEntity
    {
        public string LastName { get; set; }
        public string FirstMidName { get; set; }
    }
}