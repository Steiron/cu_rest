﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities
{
    public class Instructor : Person
    {
        public DateTime HireDate { get; set; }

        public ICollection<CourseAssignment> CourseAssignments { get; set; }
        public OfficeAssignment OfficeAssignment { get; set; }
    }
}