﻿using System.Collections.Generic;

namespace ApplicationCore.Interfaces.PagedList
{
    public interface IPagedList<TEntity>
    {
        int PageIndex { get; }
        int PageSize { get; }
        int TotalCount { get; }
        int TotalPages { get; }
        IList<TEntity> Items { get; }
    }
}