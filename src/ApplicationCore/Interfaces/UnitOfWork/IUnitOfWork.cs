﻿using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repositories.Base;

namespace ApplicationCore.Interfaces.UnitOfWork
{
    public interface IUnitOfWork
    {
        /// <summary>
        /// Служит для получения IGenericRepository репозитория сущности типа {TEntity}
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <returns>Возвращает IGenericRepository</returns>

        IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity;

        /// <summary>
        /// Служит для получения интерфейса репозитория заданного
        /// в параметре {TRepository}
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <typeparam name="TRepository">Интерфейс репозитория {IGenericRepository{TEntity}}</typeparam>
        /// <returns>Возвращает заданный {TRepository}</returns>

        TRepository GetRepositoryWithTypeRepository<TEntity, TRepository>()
            where TEntity : BaseEntity
            where TRepository : IGenericRepository<TEntity>;

        int Save();
        Task<int> SaveAsync();
    }
}