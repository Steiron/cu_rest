﻿using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.PagedList;
using ApplicationCore.Interfaces.Repositories.Base;

namespace ApplicationCore.Interfaces.Repositories
{
    public interface IStudentRepository : IGenericRepository<Student>
    {
        /// <summary>
        /// Ищет сущность <see cref="Student"/> включая данные <see cref="Enrollment"/> и <see cref="Course"/> по id.
        /// Если найдена возвращается сущность. Если не найдена возвращается null.
        /// </summary>
        /// <param name="id">Первичный ключ</param>
        /// <returns>Возвращает сущность <see cref="Student"/> или null</returns>
        Task<Student> GetStudentByIdWithEnrollmentsAndCourseAsync(int id);

        Task<IPagedList<Student>> GetAllWithFilter(string sortOrder, string searchString, int page, int pageSize);

    }
}