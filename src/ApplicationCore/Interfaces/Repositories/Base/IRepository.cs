﻿using System.Collections.Generic;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.Repositories.Base
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        TEntity GetById(int id);
        IList<TEntity> GetAll();
        TEntity Add(TEntity enity);
    }
}