﻿using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.Repositories.Base
{
    public interface IGenericRepository<TEntity> : IRepository<TEntity>, IAsyncRepository<TEntity>
        where TEntity : BaseEntity
    {
        TEntity Update(TEntity entity);
        void Delete(TEntity entity);
    }
}