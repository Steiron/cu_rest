﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.PagedList;
using Microsoft.EntityFrameworkCore.Query;

namespace ApplicationCore.Interfaces.Repositories.Base
{
    public interface IAsyncRepository<TEntity> where TEntity : BaseEntity
    {
        Task<TEntity> GetByIdAsync(int id);
        Task<IList<TEntity>> GetAllAsync();
        Task<TEntity> AddAsync(TEntity entity);

        Task<IList<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
            bool disableTracking = true);

        /// <summary>
        /// Возвращает заданное количество элементов на странице
        /// включая predicate, orderby, include.
        /// По умолчанию используется no-tracking query.
        /// </summary>
        /// <param name="predicate">Условия поиска</param>
        /// <param name="orderBy">Условия сортировки</param>
        /// <param name="include">Услвоия связанных данных</param>
        /// <param name="pageIndex">Номер страницы</param>
        /// <param name="pageSize">Количество элементов на странице</param>
        /// <param name="disableTracking">Отключает отслеживание изменений <c>AsNoTracking</c>. По умолчанию <c>True</c>.</param>
        /// <returns>Возвращает список элементов по заданным условия</returns>
        Task<IPagedList<TEntity>> GetPagedListAsync(Expression<Func<TEntity, bool>> predicate = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
            int pageIndex = 0,
            int pageSize = 20,
            bool disableTracking = true);

        /// <summary>
        /// Используется метод LINQ GetFirstOrDefault, включая predicate, orderby, include.
        /// По умолчанию используется no-tracking query.
        /// </summary>
        /// <param name="predicate">Условия поиска</param>
        /// <param name="orderBy">Сортировка по возрастанию</param>
        /// <param name="include">Усвлоия связанных данных</param>
        /// <param name="disableTracking">Отключает отслеживание изменений <c>AsNoTracking</c>. По умолчанию <c>True</c>.</param>
        /// <returns>Возвращает элемент по заданным условиям</returns>
        Task<TEntity> GetFirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
            bool disableTracking = true);
    }
}