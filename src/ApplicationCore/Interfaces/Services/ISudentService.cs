﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.DTO.Student;
using ApplicationCore.Interfaces.PagedList;
using ApplicationCore.Interfaces.Repositories;
using ApplicationCore.Interfaces.Repositories.Base;
using ApplicationCore.Services;

namespace ApplicationCore.Interfaces.Services
{
    public interface ISudentService
    {
        Task<StudentDto> GetByIdAsync(int id);

        /// <summary>
        /// Использует метод <see cref="IStudentRepository.GetStudentByIdWithEnrollmentsAndCourseAsync"/> из репозитория
        /// <see cref="IStudentRepository"/>
        /// и преобразовывает в <see cref="StudentDto"/> через Automapper
        /// </summary>
        /// <param name="id">Первичный ключ</param>
        /// <returns>Возвращает <see cref="StudentDto"/> или null</returns>
        Task<StudentEnrollmentsDto> GetStudentByIdWithEnrollmentsAsync(int id);

        /// <summary>
        /// Использует метод <see cref="IStudentRepository.GetPagedListAsync"/> и преобразовывает в IPagedList <see cref="StudentDto"/>
        /// через Automapper
        /// </summary>
        /// <param name="sortOrder">Условия для сортировки</param>
        /// <param name="searchString">Условия для поиска</param>
        /// <param name="page">Номер страницы</param>
        /// <param name="pageSize">Количество элементов на странице</param>
        /// <returns></returns>
        Task<IPagedList<StudentDto>> GetPagedListAsync(string sortOrder, string searchString, int? page, int pageSize);

        Task<StudentDto> CreateStudentAsync(StudentDto studentDto);
        Task<StudentDto> UpdateStudentAsync(int id, StudentDto studentDto);
        Task<StudentDto> DeleteStudentAsync(int id);
    }
}