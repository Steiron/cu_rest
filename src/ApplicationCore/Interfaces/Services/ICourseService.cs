﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;

namespace ApplicationCore.Interfaces.Services
{
    public interface ICourseService
    {
        Task<IList<CourseDto>> GetAllWithDepartmentAsync();
        Task<CourseDto> GetByIdWithDepartmentAsync(int id);
        Task<CourseDto> GetById(int id);
        Task<CourseDto> CreateCourseAsync(CourseDto courseDto);
        Task<CourseDto> UpdateCourseAsync(int id, CourseDto courseDto);
        Task<CourseDto> DeleteCourseAsync(int id);
    }
}