﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;

namespace ApplicationCore.Interfaces.Services
{
    public interface IDepartmentService
    {
        Task<IList<DepartmentDto>> GetAllWithAdministratorAsync();
        Task<DepartmentDto> GetById(int id);
        Task<DepartmentDto> GetDetailsById(int id);
        Task<DepartmentDto> CreateDepartmentAsync(DepartmentDto departmentDto);
        Task<DepartmentDto> UpdateDepartmentAsync(int id, DepartmentDto departmentDto);
        Task<DepartmentDto> DeleteDepartmentAsync(int id);
    }
}