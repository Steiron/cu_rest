﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;

namespace ApplicationCore.Interfaces.Services
{
    public interface IInstructorService
    {
        Task<IEnumerable<InstructorDto>> GetAllWithCoursesAsync();
        Task<InstructorDto> GetById(int id);
    }
}