﻿using ApplicationCore.DTO;
using ApplicationCore.DTO.Student;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.PagedList;
using ApplicationCore.Services;
using AutoMapper;

namespace ApplicationCore.Mappers
{
    public class DomainProfile : Profile
    {
        public DomainProfile()
        {
            CreateMap<BaseEntity, BaseDto>()
                .Include<Person, PersonDto>()
                .Include<Enrollment, EnrollmentDto>()
                .Include<Department, DepartmentDto>();

            CreateMap<Person, PersonDto>()
                .Include<Student, StudentDto>()
                .Include<Instructor, InstructorDto>();

            CreateMap<Student, StudentDto>()
                .Include<Student, StudentEnrollmentsDto>();

            CreateMap<Student, StudentEnrollmentsDto>();
            CreateMap<Enrollment, EnrollmentDto>().MaxDepth(1);
            CreateMap<Department, DepartmentDto>().MaxDepth(1);
            CreateMap<Course, CourseDto>().MaxDepth(1);
            CreateMap<CourseAssignment, CourseAssignmentDto>().MaxDepth(1);
            CreateMap<Instructor, InstructorDto>();
            CreateMap<OfficeAssignment, OfficeAssignmentDto>();
            CreateMap(typeof(PagedList<>), typeof(IPagedList<>));
            CreateMap(typeof(IPagedList<>), typeof(PagedList<>));
        }
    }
}