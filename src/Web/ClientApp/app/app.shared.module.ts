import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';

import {AppComponent} from './components/app/app.component';
import {NavMenuComponent} from './components/navmenu/navmenu.component';
import {HomeComponent} from './components/home/home.component';
import {StudentsComponent} from "./components/student/students.component";
import {StudentCreateComponent} from "./components/student/create/student-create.component";
import {StudentEditComponent} from "./components/student/edit/student-edit.component";
import {StudentDeleteComponent} from "./components/student/delete/student-delete.component";
import {StudentDetailsComponent} from "./components/student/details/student-details.component";
import {NgxPaginationModule} from "ngx-pagination";
import {CoursesComponent} from "./components/courses/list/courses.component";
import {CourseCreateComponent} from "./components/courses/create/course-create.component";
import {CourseDetailsComponent} from "./components/courses/details/course-details.component";
import {CourseEditComponent} from "./components/courses/edit/course-edit.component";
import {CourseDeleteComponent} from "./components/courses/delete/course-delete.component";
import {DepartmentsComponent} from "./components/department/list/departments.component";
import {DepartmentCreateComponent} from "./components/department/create/department-create.component";
import {DepartmentDeleteComponent} from "./components/department/delete/department-delete.component";
import {DepartmentEditComponent} from "./components/department/edit/department-edit.component";
import {InstructorsComponent} from "./components/instructor/list/instructors.component";

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        StudentsComponent,
        StudentEditComponent,
        StudentDeleteComponent,
        StudentCreateComponent,
        StudentDetailsComponent,
        CoursesComponent,
        CourseCreateComponent,
        CourseDetailsComponent,
        CourseEditComponent,
        CourseDeleteComponent,
        DepartmentsComponent,
        DepartmentCreateComponent,
        DepartmentEditComponent,
        DepartmentDeleteComponent,
        InstructorsComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        RouterModule.forRoot([
            {path: '', redirectTo: 'home', pathMatch: 'full'},
            {path: 'home', component: HomeComponent},
            {path: 'student', component: StudentsComponent},
            {path: 'student/create', component: StudentCreateComponent},
            {path: 'student/edit/:id', component: StudentEditComponent},
            {path: 'student/delete/:id', component: StudentDeleteComponent},
            {path: 'student/details/:id', component: StudentDetailsComponent},
            {path: 'course', component: CoursesComponent},
            {path: 'course/create', component: CourseCreateComponent},
            {path: 'course/edit/:id', component: CourseEditComponent},
            {path: 'course/details/:id', component: CourseDetailsComponent},
            {path: 'course/delete/:id', component: CourseDeleteComponent},
            {path: 'department', component: DepartmentsComponent},
            {path: 'department/create', component: DepartmentCreateComponent},
            {path: 'department/edit/:id', component: DepartmentEditComponent},
            {path: 'department/delete/:id', component: DepartmentDeleteComponent},
            {path: 'instructor', component: InstructorsComponent},
            {path: '**', redirectTo: 'home'}
        ])
    ]
})
export class AppModuleShared {
}
