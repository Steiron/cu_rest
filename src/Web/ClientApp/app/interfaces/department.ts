﻿interface Department extends BaseEntity {
    name: string;
    budget: number;
    startDate: Date
    instructorID: number;
    courses: Course[];
    administrator: Instructor
}