﻿interface Enrollment extends BaseEntity {
    courseID: number;
    studentID: number;
    grade: string;
    course: Course;
}
