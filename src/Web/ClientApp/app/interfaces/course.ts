﻿interface Course extends BaseEntity {
    title: string;
    credits: number;
    departmentID: number;
    department: Department;
    enrollments: Enrollment[];
    courseAssignments: CourseAssignment[];
}