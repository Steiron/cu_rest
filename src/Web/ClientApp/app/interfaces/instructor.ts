﻿interface Instructor extends Person {
    officeAssignment: OfficeAssignment;
    courseAssignments: CourseAssignment[]
    hireDate: Date;
}