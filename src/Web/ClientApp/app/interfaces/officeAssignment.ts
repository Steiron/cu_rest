﻿interface OfficeAssignment {
    instructorID: number;
    location: string;
    instructor: Instructor;
}