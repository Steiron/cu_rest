﻿import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";


@Injectable()
export class CourseService {
    url: string = "http://localhost:5000/api/v1/courses";

    constructor(private _http: Http) {
    }

    getCourses() {
        return this._http.get(this.url)
            .map(resp => resp.json())
            .catch(this.errorHandler);
    }

    getCourseById(id: number) {
        return this._http.get(`${this.url}/${id}`)
            .map(response => response.json())
            .catch(this.errorHandler);
    }

    createCourse(course: Course) {
        return this._http.post(this.url, course)
            .map(response => response.json())
            .catch(this.errorHandler);
    }

    getCourseDetailsById(id: number) {
        return this._http.get(`${this.url}/details/${id}`)
            .map(resp => resp.json())
            .catch(this.errorHandler);
    }

    updateCourse(course: Course) {
        return this._http.put(`${this.url}/${course.id}`, course)
            .map(resp => resp.json())
            .catch(this.errorHandler);
    }

    deleteCourse(id: number) {
        return this._http.delete(`${this.url}/${id}`)
            .catch(this.errorHandler);
    }

    errorHandler(error: Response) {
        console.error(error);
        return Observable.throw(error);
    }
}