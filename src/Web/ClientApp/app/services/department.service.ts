﻿import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";


@Injectable()
export class DepartmentService {
    url: string = "http://localhost:5000/api/v1/departments";

    constructor(private _http: Http) {
    }

    getDepartments() {
        return this._http.get(this.url)
            .map(resp => resp.json())
            .catch(this.errorHandler);
    }

    getDepartmentById(id: number) {
        return this._http.get(`${this.url}/${id}`)
            .map(response => response.json())
            .catch(this.errorHandler);
    }

    getDepartmentDetailsById(id: number) {
        return this._http.get(`${this.url}/details/${id}`)
            .map(response => response.json())
            .catch(this.errorHandler);
    }

    createDepartment(department: Department) {
        return this._http.post(this.url, department)
            .map(response => response.json())
            .catch(this.errorHandler);
    }

    updateDepartment(department: Department) {
        return this._http.put(`${this.url}/${department.id}`, department)
            .map(response => response.json())
            .catch(this.errorHandler);
    }

    deleteDepartment(id: number) {
        return this._http.delete(`${this.url}/${id}`)
            .catch(this.errorHandler);
    }


    errorHandler(error: Response) {
        console.error(error);
        return Observable.throw(error);
    }
}