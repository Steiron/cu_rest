﻿import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";


@Injectable()
export class InstructorService {
    url: string = "http://localhost:5000/api/v1/instructors";

    constructor(private _http: Http) {
    }

    getInstructors() {
        return this._http.get(this.url)
            .map(resp => resp.json())
            .catch(this.errorHandler);
    }

    getInstructorById(id: number) {
        return this._http.get(`${this.url}/${id}`)
            .map(response => response.json())
            .catch(this.errorHandler);
    }

    createInstructor(instructor: Instructor) {
        return this._http.post(this.url, instructor)
            .map(response => response.json())
            .catch(this.errorHandler);
    }

    errorHandler(error: Response) {
        console.error(error);
        return Observable.throw(error);
    }
}