﻿import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from "rxjs/Observable";


@Injectable()
export class StudentService {
    url: string = "http://localhost:5000/api/v1/students";

    constructor(private _http: Http) {
    }

    getStudents(page: number, filter: string) {
        return this._http.get(`${this.url}?page=${page}&searchString=${filter}`)
            .map(response => response.json())
            .catch(this.errorHandler);
    }

    getStudentById(id: number) {
        return this._http.get(`${this.url}/${id}`)
            .map(response => response.json())
            .catch(this.errorHandler);
    }

    getStudentDetalilById(id: number) {
        return this._http.get(`${this.url}/details/${id}`)
            .map(response => response.json())
            .catch(this.errorHandler);
    }

    createStudent(student: Student) {
        return this._http.post(this.url, student)
            .map(response => response.json())
            .catch(this.errorHandler);
    }

    updateStudent(student: Student) {
        return this._http.put(`${this.url}/${student.id}`, student)
            .map(response => response.json())
            .catch(this.errorHandler);
    }

    deleteStudent(id: number) {
        return this._http.delete(`${this.url}/${id}`)
            .catch(this.errorHandler);
    }

    errorHandler(error: Response) {
        console.error(error);
        return Observable.throw(error);
    }
}