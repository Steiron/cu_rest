﻿import {Component, OnInit} from "@angular/core";
import {CourseService} from "../../../services/course.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {DepartmentService} from "../../../services/department.service";


@Component({
    templateUrl: "./course-create.component.html",
    providers: [CourseService, DepartmentService]
})

export class CourseCreateComponent implements OnInit {
    courseForm: FormGroup;
    errorMessage: any;
    departments: Department[];

    constructor(private _fb: FormBuilder,
                private _ar: ActivatedRoute,
                private _router: Router,
                private _courseService: CourseService,
                private _departmentService: DepartmentService) {

        this.courseForm = this._fb.group({
            id: [0, [Validators.required]],
            title: ['', [Validators.required]],
            credits: ['', [Validators.required, Validators.min(0), Validators.max(5)]],
            departmentID: [-1, [Validators.required, Validators.min(1)]]
        })
    }

    ngOnInit(): void {
        this._departmentService.getDepartments().subscribe(value => this.departments = value);
    }

    onSave() {
        if (!this.courseForm.valid) {
            return;
        }
        this._courseService.createCourse(this.courseForm.value)
            .subscribe(data => this._router.navigate(['/course']),
                error1 => this.errorMessage = error1);
    }

    get id() {
        return this.courseForm.get("id");
    }
}