﻿import {Component, OnInit} from "@angular/core";
import {CourseService} from "../../../services/course.service";
import {ActivatedRoute, Router} from "@angular/router";


@Component({
    templateUrl: "./course-details.component.html",
    providers: [CourseService]
})

export class CourseDetailsComponent implements OnInit {
    errorMessage: any;
    id: number;
    course: Course;

    constructor(private _ar: ActivatedRoute,
                private _routet: Router,
                private _courseService: CourseService) {
        this.id = +this._ar.snapshot.params['id'];
    }

    ngOnInit(): void {
        this._courseService.getCourseDetailsById(this.id).subscribe(
            value => this.course = value,
            error1 => this.errorMessage = error1);

    }

}