﻿import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {CourseService} from "../../../services/course.service";


@Component({
    templateUrl: "./course-delete.component.html",
    providers: [CourseService]
})

export class CourseDeleteComponent implements OnInit {
    errorMessage: any;
    course: Course;
    id: number;

    constructor(private _ar: ActivatedRoute,
                private _router: Router,
                private _courseService: CourseService) {

        this.id = +this._ar.snapshot.params['id'];
    }

    ngOnInit() {
        this._courseService.getCourseDetailsById(this.id).subscribe(
            data => this.course = data,
            error1 => this.errorMessage = error1);
    }

    onSubmit() {
        this._courseService.deleteCourse(this.course.id).subscribe(data => {
            this._router.navigate(['/course']);
        }, error1 => this.errorMessage = error1);
    }
}

