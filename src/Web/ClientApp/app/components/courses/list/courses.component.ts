﻿import {Component, OnInit} from "@angular/core";
import {CourseService} from "../../../services/course.service";
import {Router} from "@angular/router";
import {Http} from "@angular/http";


@Component({
    selector: "courses",
    templateUrl: "./courses.component.html",
    providers: [CourseService]
})

export class CoursesComponent implements OnInit {
    courses: Course[];

    constructor(private _router: Router,
                private _http: Http,
                private _courseService: CourseService) {
    }

    ngOnInit(): void {
        this._courseService.getCourses().subscribe(value => this.courses = value);
    }

}