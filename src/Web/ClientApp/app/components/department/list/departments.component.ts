﻿import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {Http} from "@angular/http";
import {DepartmentService} from "../../../services/department.service";

@Component({
    templateUrl: "./departments.component.html",
    providers: [DepartmentService]
})

export class DepartmentsComponent implements OnInit {
    departments: Department[];

    constructor(private _router: Router,
                private _http: Http,
                private _depatmentService: DepartmentService) {
    }


    ngOnInit(): void {
        this._depatmentService.getDepartments()
            .subscribe(value => this.departments = value)

    }

}