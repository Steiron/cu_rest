﻿import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {DepartmentService} from "../../../services/department.service";
import {InstructorService} from "../../../services/instructor.service";


@Component({
    templateUrl: "./department-create.component.html",
    providers: [DepartmentService, InstructorService]
})

export class DepartmentCreateComponent implements OnInit {
    depForm: FormGroup;
    errorMessage: any;
    instructors: Instructor[];

    constructor(private _fb: FormBuilder,
                private _ar: ActivatedRoute,
                private _router: Router,
                private _instructorService: InstructorService,
                private _departmentService: DepartmentService) {

        this.depForm = this._fb.group({
            name: ['', [Validators.required]],
            budget: ['', [Validators.required]],
            startDate: ['', [Validators.required]],
            instructorId: [-1, [Validators.required, Validators.min(1)]]
        })
    }

    ngOnInit(): void {
        this._instructorService.getInstructors().subscribe(value => this.instructors = value);
    }

    onSave() {
        if (!this.depForm.valid) {
            return;
        }
        this._departmentService.createDepartment(this.depForm.value).subscribe(
            data => this._router.navigate(['/department']),
            error1 => this.errorMessage = error1);
    }

}