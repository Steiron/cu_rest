﻿import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {DepartmentService} from "../../../services/department.service";


@Component({
    templateUrl: "./department-delete.component.html",
    providers: [DepartmentService]
})

export class DepartmentDeleteComponent implements OnInit {
    errorMessage: any;
    dep: Department;
    id: number;

    constructor(private _ar: ActivatedRoute,
                private _router: Router,
                private _depService: DepartmentService) {

        this.id = +this._ar.snapshot.params['id'];
    }

    ngOnInit() {
        this._depService.getDepartmentDetailsById(this.id).subscribe(
            data => this.dep = data,
            error1 => this.errorMessage = error1);
    }

    onSubmit() {
        this._depService.deleteDepartment(this.dep.id).subscribe(data => {
            this._router.navigate(['/department']);
        }, error1 => this.errorMessage = error1);
    }
}

