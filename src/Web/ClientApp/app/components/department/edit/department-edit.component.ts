﻿import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {DepartmentService} from "../../../services/department.service";
import {InstructorService} from "../../../services/instructor.service";


@Component({
    templateUrl: "./department-edit.component.html",
    providers: [DepartmentService, InstructorService]
})

export class DepartmentEditComponent implements OnInit {
    depForm: FormGroup;
    errorMessage: any;
    instructors: Instructor[];
    id: number;

    constructor(private _fb: FormBuilder,
                private _ar: ActivatedRoute,
                private _router: Router,
                private _instructorService: InstructorService,
                private _departmentService: DepartmentService) {

        this.id = +_ar.snapshot.params['id'];

        this.depForm = this._fb.group({
            id: 0,
            name: ['', [Validators.required]],
            budget: ['', [Validators.required]],
            startDate: ['', [Validators.required]],
            instructorID: [-1, [Validators.required, Validators.min(1)]]
        })
    }

    ngOnInit(): void {
        this._departmentService.getDepartmentById(this.id).subscribe(value => this.depForm.setValue(value));
        this._instructorService.getInstructors().subscribe(value => this.instructors = value);
    }

    onSave() {
        if (!this.depForm.valid) {
            return;
        }
        this._departmentService.updateDepartment(this.depForm.value).subscribe(
            data => this._router.navigate(['/department']),
            error1 => this.errorMessage = error1);
    }

}