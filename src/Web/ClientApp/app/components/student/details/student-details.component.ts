﻿import {Component, OnInit} from "@angular/core";
import {StudentService} from "../../../services/student.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder} from "@angular/forms";
import {Title} from "@angular/platform-browser";


@Component({
    templateUrl: "./student-details.component.html",
    providers: [StudentService]
})

export class StudentDetailsComponent implements OnInit {
    errorMessage: any;
    id: number;
    studentDetails: StudentDetails;

    constructor(private _fb: FormBuilder,
                private activeRoute: ActivatedRoute,
                private router: Router,
                private _studentService: StudentService,
                private _titleService: Title) {

        this.id = +this.activeRoute.snapshot.params['id'];
    }


    ngOnInit() {
        this._studentService.getStudentDetalilById(this.id).subscribe(
            data => this.studentDetails = data,
            error1 => this.errorMessage = error1);
    }

}