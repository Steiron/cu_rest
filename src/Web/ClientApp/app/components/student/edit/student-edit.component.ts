import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Title} from "@angular/platform-browser";
import {StudentService} from "../../../services/student.service";


@Component({
    templateUrl: "./student-edit.component.html",
    providers: [StudentService]
})

export class StudentEditComponent implements OnInit {
    studentForm: FormGroup;
    errorMessage: any;
    id: number;

    constructor(private _fb: FormBuilder,
                private activeRoute: ActivatedRoute,
                private router: Router,
                private _studentService: StudentService,
                private _titleService: Title) {
        // this._titleService.setTitle('Edit');
        this.id = +this.activeRoute.snapshot.params['id'];
        this.studentForm = this._fb.group({
            id: 0,
            first: ['', [Validators.required, Validators.maxLength(50)]],
            last: ['', [Validators.required, Validators.maxLength(50)]],
            enrollmentDate: ['', [Validators.required]]
        });
    }

    ngOnInit() {
        this._studentService.getStudentById(this.id).subscribe(
            data => this.studentForm.setValue(data),
            error1 => this.errorMessage = error1);
    }

    onSave() {
        if (!this.studentForm.valid) {
            return;
        }
        this._studentService.updateStudent(this.studentForm.value)
            .subscribe((data) => {
                this.router.navigate(['/students']);
            }, error1 => this.errorMessage = error1);
    }

    onBack() {
        this.router.navigate(["students"]);
    }

    get first() {
        return this.studentForm.get('first');
    }

    get last() {
        return this.studentForm.get('last');
    }

    get enrollmentDate() {
        return this.studentForm.get('enrollmentDate');
    }

}