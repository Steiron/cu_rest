﻿import {Component} from "@angular/core";
import {StudentService} from "../../../services/student.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Title} from "@angular/platform-browser";


@Component({
    templateUrl: "./student-create.component.html",
    providers: [StudentService]
})

export class StudentCreateComponent {
    studentForm: FormGroup;
    errorMessage: any;

    constructor(private _fb: FormBuilder,
                private activeRoute: ActivatedRoute,
                private router: Router,
                private _studentService: StudentService,
                private _titleService: Title) {
        // this._titleService.setTitle('Edit');
        this.studentForm = this._fb.group({
            id: 0,
            first: ['', [Validators.required, Validators.maxLength(50)]],
            last: ['', [Validators.required, Validators.maxLength(50)]],
            enrollmentDate: ['', [Validators.required]]
        });
    }

    onSave() {
        if (!this.studentForm.valid) {
            return;
        }
        this._studentService.createStudent(this.studentForm.value)
            .subscribe(data => {
                this.router.navigate(['/student']);
            }, error1 => this.errorMessage = error1);
    }

    onBack() {
        this.router.navigate(["student"]);
    }

    get first() {
        return this.studentForm.get('first');
    }

    get last() {
        return this.studentForm.get('last');
    }

    get enrollmentDate() {
        return this.studentForm.get('enrollmentDate');
    }
}