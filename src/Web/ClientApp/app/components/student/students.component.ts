import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {Http} from "@angular/http";
import {StudentService} from "../../services/student.service";

@Component({
    selector: "students",
    templateUrl: "./students.component.html",
    providers: [StudentService]
})

export class StudentsComponent implements OnInit {
    students: Student[];
    itemPerPage: number = 3;
    pageIndex: number = 1;
    total: number;
    searchString: string = '';

    constructor(private _router: Router,
                public http: Http,
                private _studentService: StudentService) {
    }

    ngOnInit() {
        this.getPage(this.pageIndex);
    }

    getPage(page: number) {
        this._studentService.getStudents(page, this.searchString).subscribe(data => {
            this.students = data.items;
            this.pageIndex = data.pageIndex;
            this.total = data.totalCount;
        });
    }

    getFilter() {
        this.getPage(this.pageIndex);
    }
}