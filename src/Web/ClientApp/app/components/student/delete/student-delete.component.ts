﻿import {Component, OnInit} from "@angular/core";
import {StudentService} from "../../../services/student.service";
import {ActivatedRoute, Router} from "@angular/router";


@Component({
    templateUrl: "./student-delete.component.html",
    providers: [StudentService]
})

export class StudentDeleteComponent implements OnInit {
    errorMessage: any;
    student: Student;
    id: number;

    constructor(private _ar: ActivatedRoute,
                private _router: Router,
                private _studentService: StudentService) {

        this.id = +this._ar.snapshot.params['id'];
    }

    ngOnInit() {
        this._studentService.getStudentById(this.id).subscribe(
            data => this.student = data,
            error1 => this.errorMessage = error1);
    }

    onSubmit() {
        this._studentService.deleteStudent(this.student.id).subscribe(data => {
            this._router.navigate(['/student']);
        }, error1 => this.errorMessage = error1);
    }
}

