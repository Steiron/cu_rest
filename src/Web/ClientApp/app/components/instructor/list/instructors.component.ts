﻿import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Http} from "@angular/http";
import {InstructorService} from "../../../services/instructor.service";

@Component({
    templateUrl: "./instructors.component.html",
    providers: [InstructorService]
})

export class InstructorsComponent implements OnInit {
    instructors: Instructor[];
    courses: Course[];

    constructor(private _router: Router,
                private _ar: ActivatedRoute,
                private _http: Http,
                private _instructorService: InstructorService) {

    }


    ngOnInit(): void {
        this._instructorService.getInstructors()
            .subscribe(value => this.instructors = value)

    }

    isSelect(id: number): void {
        console.log(id);

    }

}