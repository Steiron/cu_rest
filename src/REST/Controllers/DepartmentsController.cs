﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace REST.Controllers
{
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("/api/v{api-version:apiVersion}/[controller]")]
    public class DepartmentsController : Controller
    {
        private readonly IDepartmentService _service;

        #region Constructor

        public DepartmentsController(IDepartmentService service)
        {
            _service = service;
        }

        #endregion

        #region GET /departments

        [HttpGet]
        public async Task<IList<DepartmentDto>> GetAll()
        {
            return await _service.GetAllWithAdministratorAsync();
        }

        #endregion

        #region GET /departments/id

        [HttpGet("{id:int}", Name = "GetDepartment")]
        public async Task<IActionResult> GetById(int id)
        {
            var department = await _service.GetById(id);
            if (department == null) return NotFound($"Не найден по id:{id}");
            return new ObjectResult(department);
        }

        #endregion

        #region GET /departments/details/id

        [HttpGet("details/{id:int}")]
        public async Task<IActionResult> GetDetailsById(int id)
        {
            var department = await _service.GetDetailsById(id);
            if (department == null) return NotFound($"Не найден курс по id:{id}");
            return new ObjectResult(department);
        }

        #endregion

        #region POST /departments

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] DepartmentDto departmentDto)
        {
            if (departmentDto == null) return BadRequest(ModelState);
            try
            {
                if (ModelState.IsValid)
                {
                    var newDepartment = await _service.CreateDepartmentAsync(departmentDto);
                    return CreatedAtRoute("GetDepartment", new {id = newDepartment.Id}, newDepartment);
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion


        #region PUT /departments/id

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Update(int id, [FromBody] DepartmentDto departmentDto)
        {
            if (departmentDto == null) return BadRequest(ModelState);
            try
            {
                if (ModelState.IsValid)
                {
                    var departmentUpdate = await _service.UpdateDepartmentAsync(id, departmentDto);
                    if (departmentUpdate == null) return NotFound($"Не найден курс по id:{id}");
                    return CreatedAtRoute("GetDepartment", new {id = departmentUpdate.Id}, departmentUpdate);
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion

        #region DELETE /department/id

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var department = await _service.DeleteDepartmentAsync(id);
                if (department == null) return NotFound($"Не найден курс по id:{id}");
                return NoContent();
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion
    }
}