﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace REST.Controllers
{
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("/api/v{api-version:apiVersion}/[controller]")]
    public class InstructorsController : Controller
    {
        private readonly IInstructorService _service;

        public InstructorsController(IInstructorService service)
        {
            _service = service;
        }

        #region GET /instructors

        [HttpGet]
        public async Task<IEnumerable<InstructorDto>> GetAll()
        {
            return await _service.GetAllWithCoursesAsync();
        }

        #endregion

        #region GET /instrutcors/id

        [HttpGet("{id:int}", Name = "GetInstructor")]
        public async Task<IActionResult> GetById(int id)
        {
            var instructor = await _service.GetById(id);
            if (instructor == null) return NotFound();
            return new ObjectResult(instructor);
        }

        #endregion
    }
}