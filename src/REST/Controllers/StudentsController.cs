﻿using System;
using System.Threading.Tasks;
using ApplicationCore.DTO.Student;
using ApplicationCore.Interfaces.PagedList;
using ApplicationCore.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using REST.Swagger.Student;
using Swashbuckle.AspNetCore.Examples;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace REST.Controllers

{
    [Produces("application/json")]
    [ApiVersion("1.0", Deprecated = true)]
    [Route("/api/v{api-version:apiVersion}/[controller]")]
    public class StudentsController : Controller
    {
        private readonly ISudentService _service;

        #region Constructor

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="service"></param>
        public StudentsController(ISudentService service)
        {
            _service = service;
        }

        #endregion

        #region GET /students

        ///  <summary>
        ///  Возвращает коллекцию студентов на одной странице.
        ///  </summary>
        ///  <remarks>
        ///  Пример запроса:
        /// 
        ///      GET: /students - вернет элементы с 1 страницы
        ///      GET: /students?page=2 - вернет элементы с 2 страницы
        ///  
        ///  </remarks>
        /// <param name="searchString"></param>
        /// <param name="page">Номер страницы. <c>По умолчанию 1</c></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [SwaggerResponseExample(200, typeof(StudentListDtoExpamle))]
        [SwaggerResponse(200, typeof(IPagedList<StudentDto>), "Возвращает коллекцию студентов")]
        [HttpGet]
        public async Task<IPagedList<StudentDto>> GetAll(string sortOrder,
            string searchString,
            int? page,
            int pageSize = 3)
        {
            return await _service.GetPagedListAsync(sortOrder, searchString, page, pageSize);
        }

        #endregion

        #region GET /students/id

        /// <summary>
        /// Возвращает студента по заданному id
        /// </summary>
        /// <remarks>
        /// Пример запроса:
        /// 
        ///     GET /students/1
        /// 
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Возвращает студента</returns>
        [HttpGet("{id}", Name = "GetStudent")]
        [SwaggerResponse(200, typeof(StudentDto), "Возвращает студента")]
        [SwaggerResponse(404, null, "Cтудент не найден")]
        public async Task<IActionResult> GetById(int id)
        {
            var student = await _service.GetByIdAsync(id);
            if (student == null) return NotFound($"Не найден студент по заданному id:{id}");
            return new ObjectResult(student);
        }

        #endregion

        #region GET /students/details/id

        /// <summary>
        /// Возвращает студента списком Enrollments и Course по заданному id
        /// </summary>
        /// <remarks>
        /// Пример запроса:
        ///
        ///     GET /students/details/1
        /// 
        /// </remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("details/{id}", Name = "GetStudentWithEnrollments")]
        [SwaggerResponse(200, typeof(StudentEnrollmentsDto), "Возвращает студента списком Enrollments и Course")]
        [SwaggerResponse(404, null, "Cтудент не найден")]
        public async Task<IActionResult> GetDetailsById(int id)
        {
            var student = await _service.GetStudentByIdWithEnrollmentsAsync(id);
            if (student == null) return NotFound($"Не найден студент по заданному id:{id}");
            return new ObjectResult(student);
        }

        #endregion

        #region POST /students

        /// <summary>
        /// Создает студента
        /// </summary>
        ///  <remarks>
        /// Пример запроса:
        /// 
        ///     POST /students
        ///         {
        ///             "last": "Тестовый",
        ///             "first" : "Студент",
        ///             "enrollmentDate":"2018-01-05"
        ///         }
        /// 
        /// </remarks>
        /// <param name="studentDto"></param>
        /// <returns>Нового созданого студента</returns>
        [HttpPost]
        [SwaggerRequestExample(typeof(StudentDto), typeof(StudentDtoExample))]
        [SwaggerResponse(201, typeof(StudentDto), "Возвращает созданого студента")]
        [SwaggerResponse(400, null, "Если запрос пустой, некорректно переданы данные или ошибка при сохранение в БД")]
        public async Task<IActionResult> Create([FromBody] StudentDto studentDto)
        {
            if (studentDto == null) return BadRequest("Запрос не должен быть пустым");

            try
            {
                if (ModelState.IsValid)
                {
                    var newStudent = await _service.CreateStudentAsync(studentDto);
                    return CreatedAtRoute("GetStudent", new {id = newStudent.Id}, newStudent);
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion

        #region PUT /students/id

        /// <summary>
        /// Обновляет данные по студенту
        /// </summary>
        /// <remarks>
        /// Пример запроса:
        /// 
        ///     PUT /student/1
        ///         {
        ///             "last": "Обновленный",
        ///             "first" : "Студент",
        ///             "enrollmentDate":"2018-01-05"
        ///         }
        /// 
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="studentDto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [SwaggerResponse(201, typeof(StudentDto), "Возвращает обновленные данные по студенту")]
        [SwaggerResponse(400, null, "Если запрос пустой, некорректно переданы данные или ошибка при сохранение в БД")]
        [SwaggerResponse(404, null, "Cтудент не найден")]
        public async Task<IActionResult> Update(int id, [FromBody] StudentDto studentDto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var studentToUpdate = await _service.UpdateStudentAsync(id, studentDto);
                    if (studentToUpdate == null) return NotFound($"Student not found by id:{id}");
                    return CreatedAtRoute("GetStudent", new {id = studentToUpdate.Id}, studentToUpdate);
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion

        #region DELETE /students/id

        /// <summary>
        /// Удаляет студента по заданному id
        /// </summary>
        /// <remarks>
        /// Пример запроса:
        ///
        ///     DELETE /student/1
        /// 
        /// </remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [SwaggerResponse(204, null, "Студент успешно удален")]
        [SwaggerResponse(400, null, "Ошибка при сохранение в БД")]
        [SwaggerResponse(404, null, "Студент по заданному id не найден")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var student = await _service.DeleteStudentAsync(id);
                if (student == null) return NotFound($"Student not found by id:{id}");
                return NoContent();
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion
    }
}