﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.DTO;
using ApplicationCore.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace REST.Controllers
{
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("/api/v{api-version:apiVersion}/[controller]")]
    public class CoursesController : Controller
    {
        private readonly ICourseService _service;

        #region Constructor

        public CoursesController(ICourseService service)
        {
            _service = service;
        }

        #endregion

        #region GET /courses

        [HttpGet]
        public async Task<IList<CourseDto>> GetAll()
        {
            return await _service.GetAllWithDepartmentAsync();
        }

        #endregion

        #region GET /courses/id

        [HttpGet("{id:int}", Name = "GetCourse")]
        public async Task<IActionResult> GetById(int id)
        {
            var courses = await _service.GetById(id);
            if (courses == null) return NotFound($"Не найден курс по id:{id}");
            return new ObjectResult(courses);
        }

        #endregion

        #region GET /courses/details/id

        [HttpGet("details/{id:int}")]
        public async Task<IActionResult> GetDetailsById(int id)
        {
            var course = await _service.GetByIdWithDepartmentAsync(id);
            if (course == null) return NotFound($"Не найден курс по id:{id}");
            return new ObjectResult(course);
        }

        #endregion

        #region POST /courses

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CourseDto courseDto)
        {
            if (courseDto == null) return BadRequest(ModelState);
            try
            {
                if (ModelState.IsValid)
                {
                    var newCourse = await _service.CreateCourseAsync(courseDto);
                    return CreatedAtRoute("GetCourse", new {id = newCourse.Id}, newCourse);
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion

        #region PUT /courses/id

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Update(int id, [FromBody] CourseDto courseDto)
        {
            if (courseDto == null) return BadRequest(ModelState);
            try
            {
                if (ModelState.IsValid)
                {
                    var courseUpdate = await _service.UpdateCourseAsync(id, courseDto);
                    if (courseUpdate == null) return NotFound($"Не найден курс по id:{id}");
                    return CreatedAtRoute("GetCourse", new {id = courseUpdate.Id}, courseUpdate);
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion

        #region DELETE /couses/id

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var course = await _service.DeleteCourseAsync(id);
                if (course == null) return NotFound($"Не найден курс по id:{id}");
                return NoContent();
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
            }

            return BadRequest(ModelState);
        }

        #endregion
    }
}