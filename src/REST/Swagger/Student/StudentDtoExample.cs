﻿using System;
using ApplicationCore.DTO.Student;
using Swashbuckle.AspNetCore.Examples;

namespace REST.Swagger.Student
{
    public class StudentDtoExample : IExamplesProvider
    {
        public object GetExamples()
        {
            return new StudentDto
            {
                LastName = "Тестовый",
                FirstMidName = "Студент",
                EnrollmentDate = DateTime.Now
            };
        }
    }
}