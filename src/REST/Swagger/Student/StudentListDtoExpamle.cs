﻿using System;
using System.Collections.Generic;
using ApplicationCore.DTO.Student;
using Swashbuckle.AspNetCore.Examples;

namespace REST.Swagger.Student
{
    public class StudentListDtoExpamle : IExamplesProvider
    {
        public object GetExamples()
        {
            var students = new List<StudentDto>();
            for (int i = 0; i < 5; i++)
            {
                students.Add(new StudentDto
                {
                    LastName = "Тестовый",
                    FirstMidName = $"Студент {i}",
                    EnrollmentDate = DateTime.Now,
                    Id = i
                });
            }

            return students;
        }
    }
}