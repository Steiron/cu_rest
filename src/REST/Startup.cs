﻿using System.IO;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces.Repositories;
using ApplicationCore.Interfaces.Repositories.Base;
using ApplicationCore.Interfaces.Services;
using ApplicationCore.Interfaces.UnitOfWork;
using ApplicationCore.Services;
using AutoMapper;
using Infrastructure.Data;
using Infrastructure.Data.Repositories;
using Infrastructure.Data.UnitOfWork;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using REST.Swagger;
using Swashbuckle.AspNetCore.Examples;
using Swashbuckle.AspNetCore.Swagger;

namespace REST
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SchoolContext>(options => options.UseInMemoryDatabase("SchoolContext"));

            #region Repositories

            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddScoped<IStudentRepository, StudentRepository>();

            #endregion

            #region Services

            services.AddScoped<ISudentService, StudentService>();
            services.AddScoped<ICourseService, CourseService>();
            services.AddScoped<IDepartmentService, DepartmentService>();
            services.AddScoped<IInstructorService, InstructorService>();

            #endregion


            services.AddScoped<IUnitOfWork>(serv => new UnitOfWork(serv.GetService<SchoolContext>(), serv));

            services.AddAutoMapper();
            services.AddMvc().AddJsonOptions(o =>
            {
                o.SerializerSettings.Formatting = Formatting.Indented;
                o.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                o.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                o.SerializerSettings.ContractResolver = new EmptyCollectionContractResolver();
            });

            #region ApiVersioning

            services.AddMvcCore().AddVersionedApiExplorer(o =>
            {
                o.GroupNameFormat = "'v'VVV";
                o.SubstituteApiVersionInUrl = true;
            });

            services.AddApiVersioning(o =>
            {
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.ReportApiVersions = true;
                o.DefaultApiVersion = new ApiVersion(1, 0);
            });

            #endregion

            #region Swagger

            services.AddSwaggerGen(c =>
            {
                var provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();
                foreach (var description in provider.ApiVersionDescriptions)
                {
                    c.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));
                }

                c.IncludeXmlComments(XmlCommentPath);
                c.OperationFilter<ExamplesOperationFilter>();
            });

            #endregion

            #region CORS

            //https://docs.microsoft.com/en-us/aspnet/core/security/cors?view=aspnetcore-2.1

            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowSpecificOrigin"));
            });
            services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificOrigin",
                    builder => builder.WithOrigins("http://localhost:4000")
                        .AllowAnyHeader()
                        .AllowAnyMethod());
            });

            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApiVersionDescriptionProvider provider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();

            #region Swagger

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                foreach (var description in provider.ApiVersionDescriptions)
                {
                    c.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json",
                        $"Contoso University API {description.ApiVersion}");
                }
            });

            #endregion

            app.UseCors("AllowSpecificOrigin");
        }

        #region Доп. методы для Swagger

        private static string XmlCommentPath
        {
            get
            {
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var fileName = "CU_API.xml";
                return Path.Combine(basePath, fileName);
            }
        }

        private static Info CreateInfoForApiVersion(ApiVersionDescription description)
        {
            return new Info
            {
                Title = $"Contoso University API {description.ApiVersion}",
                Version = description.ApiVersion.ToString(),
                Description = "API примера из учебника Contoso University (Microsoft)",
                Contact = new Contact {Name = "Машнин Денис", Url = "https://t.me/Steir0n"},
            };
        }

        #endregion
    }
}